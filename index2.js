const mongoose = require('mongoose');
const compression = require("compression");
const express = require("express");
const morgan = require("morgan");
const app = express();
const config = require('./.env');
const options = config[process.env.NODE_ENV];
const _PORT = options.PORT;

mongoose.connect('mongodb://localhost/users');
const usersRouter = require("./api/users");


app.use(morgan("combined"));
app.use(express.json());
app.use("/api/users", usersRouter);
app.use(compression());

app.listen(_PORT);

