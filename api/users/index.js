const router = require("express").Router();
const controller = require("./users.controller");

router.get("/", controller.getAll);
router.get("/:id", controller.getById);
router.post("/", controller.create);
router.delete("/:id", controller.deleteUser);
router.patch("/:id", controller.changeEmail);
router.post("/:id", controller.addTweet);
//router.get("/:id/tweets/:tweetId", controller.getTweetById);



module.exports = router;