const arrayUsers = require("./users.model");
let tweetIdd = 1;

module.exports.getAll = getAll;
module.exports.getById = getById;
module.exports.create = create;
module.exports.deleteUser = deleteUser;
module.exports.changeEmail = changeEmail;
module.exports.addTweet = addTweet;

function getAll (req, res) {
    arrayUsers.find().then(response => {
        return res.json(response)
    }).catch(err => {
         res.status(500).json(err);
})
}
function deleteUser (req, res) {
    arrayUsers.find({username : req.params.id}).remove()
    .then( res.send("El usuario ha sido eliminado"))
    .catch(err => {
        return res.status(500).json(err);
    })
}
function getById(req, res ){
    arrayUsers.find({username : req.params.id}).then(response => {
        return res.json(response)
    }).catch(err => {
         res.status(500).json(err);
})
}
function create (req, res) {
    const newUser = new arrayUsers(req.body);
    const error = newUser.validateSync();
    if (!error) {
        newUser.save();
        res.json(newUser);
    } else {
        res.status(400).json(error.errors);
    }
}
function changeEmail(req, res){
    arrayUsers.update({username : req.params.id}, {$set: {email : req.body.email} }).then(function(){
        arrayUsers.find({username : req.params.id}).then(response => {
            return res.json(response)
        })
    })
}
function addTweet(req, res){
    arrayUsers.update({username : req.params.id}, { $push: {tweets: {text: req.body.tweets , owner: req.params.id , id : tweetIdd++, createdAt : Date.now().toString()}}}).then(function(){
        arrayUsers.find({username : req.params.id}).then(response => {
            return res.json(response)
        })
    })
}


