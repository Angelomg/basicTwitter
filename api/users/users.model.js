const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const TODOschema = mongoose.Schema({
    username: {
        type: String,
        required: [true, "Yeso"],
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    name : {
        type : String
    },
    tweets: {
        type: Array,
        id : {
            type : Number
        }
    }
});

TODOschema.plugin(uniqueValidator);
const users = mongoose.model('twitteruser', TODOschema);

module.exports = users;